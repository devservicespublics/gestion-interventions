<?php

namespace App\Controller;

use App\Entity\Ville;
use App\Repository\VilleRepository;
use JMS\Serializer\SerializerBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Rest\Route("/api")
 */
class VilleController extends AbstractController
{

    private $em;
    private $villeRepository;

    public function __construct(EntityManagerInterface $entityManager, 
                                VilleRepository $villeRepository)
    {
        $this->em = $entityManager;
        $this->villeRepository = $villeRepository;
    }

    /**
     * @Rest\Get("/villes", name="find_towns")
     */
    public function getAllTowns() : JsonResponse
    {
        $villes = $this->villeRepository
                        ->findOneBy(["nom_ville" => "Yaoundé"]);

                        //   ->findBy([], ["id" => "ASC"]);

        $serializer = SerializerBuilder::create()->build();
        $data = $serializer->serialize($villes , JsonEncoder::FORMAT);
        
        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }


    /**
     * @Route("/ville", name="ville")
     */
    public function index()
    {
        return $this->render('ville/index.html.twig', [
            'controller_name' => 'VilleController',
        ]);
    }
}
