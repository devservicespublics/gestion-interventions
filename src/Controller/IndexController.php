<?php
namespace App\Controller;

use Faker\Factory;
use App\Entity\Ville;
use App\Entity\Agences;
use App\Service\Geocoder;
use App\Repository\VilleRepository;
use App\Repository\AgencesRepository;
use JMS\Serializer\SerializerBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * undocumented class
 */
class IndexController extends AbstractController
{

    private $repo;
    private $villeRepository;
    private $em;
    private $urlGenerator;

    public function __construct(AgencesRepository $repoAgence, 
                                EntityManagerInterface $em,
                                VilleRepository $villeRepository,
                                UrlGeneratorInterface $urlGenerator )
    {
        $this->repo = $repoAgence;
        $this->em = $em;
        $this->villeRepository = $villeRepository;
        $this->urlGenerator = $urlGenerator;
    }
    /**
    * Creates the application.
    *
    * @return Response
    */
    public function luckyNumber()
    {
       return $this->render('security/connexion.html.twig');
    }

    /**
     * @Route("back-office/index", name="app_homepage")
     */
    public function Acceuil() 
    {

         // TODO
        // Faire une recherche dans le tableau de roles 
        // En fonction des role rediriger le l'utilisateur vers la bonne route 
        $roles = $this->getUser()->getRoles();


        if (in_array('ROLE_AGENCE', $roles)) {
            return new RedirectResponse($this->urlGenerator
                        ->generate('app_agence_homepage'));
        } 

        if (in_array('ROLE_TECHNICIEN', $roles)) {
            return new RedirectResponse($this->urlGenerator
                        ->generate('app_tech_homepage'));
        } 

        return new RedirectResponse($this->urlGenerator
                 ->generate('app_sup_homepage'));
    }
}
