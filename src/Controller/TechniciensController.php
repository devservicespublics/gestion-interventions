<?php

namespace App\Controller;

use App\Entity\Equipement;
use DateTime;
use App\Entity\RapportIntervention;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\InterventionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\DemandeInterventionRepository;
use App\Repository\RapportInterventionRepository;
use App\Service\EquipementService;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\DomCrawler\Field\TextareaFormField;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TechniciensController extends AbstractController
{

    private $em;
    private $repoDemandeIntervention;
    private $repoIntervention;
    private $rapportRepo;
    private $session;

    public function __construct(EntityManagerInterface $em, 
                                DemandeInterventionRepository $demande,
                                InterventionRepository $intervention,
                                RapportInterventionRepository $rapport,
                                SessionInterface $session)
    {
        $this->em = $em;
        $this->repoDemandeIntervention = $demande;
        $this->repoIntervention = $intervention;
        $this->rapportRepo = $rapport;
        $this->session = $session;
    }

    /**
     * @Route("back-office/technicien/mes_interventions.html", name="app_tech_homepage")
     */
    public function index()
    {
        $mesInterventions = $this->getUser()->getUtilisateur()
                                            ->getInterventions();

        $mesDemandes = array_map(function($intervention) {
            return $intervention->getDemandeIntervention();
        }, $mesInterventions->toArray());
       
        return $this->render('techniciens/index.html.twig', [
            'interventions' => $mesInterventions,
            'demande_interventions' => $mesDemandes
        ]);
    }

    /**
     * @Route("back-office/intervention/fiche_technique.html/{id}", name="technicien_intervention")
     */
    public function intervention($id)
    {

        $demandeIntervention = $this->repoDemandeIntervention
                                    ->findOneBy(['id' => $id]);

        return $this->render('techniciens/demande.html.twig', 
                             ['demande' => $demandeIntervention]);
    }

    /**
     * @Route("back-office/technicien/description_solution_intervention.html/{id}",
     * name="technicien_rapport")
     */
    public function rapportIntervention($id)
    {
            $rapportIter = new RapportIntervention();

            $demandeIntervention = $this->repoDemandeIntervention
                    ->findOneBy(['id' => $id])
                    ->getIntervention();

            $builder = $this->createFormBuilder($rapportIter)
                    ->setAction($this->generateUrl('technicien_consomables'))
                    ->setMethod('POST')
                    ->add('descriptionSolution', TextareaType::class, 
                        ['attr' => array('class' => 'cui-add-post-edit-section')])
                    ->add('etape', HiddenType::class, ['data' => '1'])
                    ->add('intervention', HiddenType::class, 
                        ['data' => $demandeIntervention->getId() ])
                    ->add('save', SubmitType::class, 
                    ['label' => 'Poster le rapport', 'attr' => array('class' => 'btn btn-sm btn-primary mr-2')])
                    // ->add('save', SubmitType::class, ['label' => 'Poste'])
                    ->getForm();

            return $this->render('techniciens/rapport-activite.html.twig', [
                'demande' => $demandeIntervention,
                'formRapport' => $builder->createView()
            ]);
    }


    /**
     * @Route("back-office/technicien/rapport_intervention.html", name="technicien_consomables")
     */
    public function setRapportIntervention(Request $request)
    {
      
       // $rapportData = $request->request->get('form');

        $rapportData  = $this->cleanData($request->request->get('form'));

        $rapport = new RapportIntervention();
        $rapport->setEtape($rapportData['etape']);
        $rapport->setDescriptionSolution($rapportData['descriptionSolution']);

        $demandeIntervention = $this->repoDemandeIntervention
                                     ->findOneBy(['id' => $rapportData['intervention']]);

        $intervention = $demandeIntervention->getIntervention();

        
        $intervention->setEtat('Terminé');
        $demandeIntervention->setStatus('Terminé');
        $intervention->setDateIntervention(new DateTime('NOW'));

        $rapport->setIntervention($intervention);

        $this->em->persist($rapport);
        $this->em->persist($demandeIntervention);
        $this->em->flush();

        return $this->redirectToRoute('technicien_equipements', 
                                        ['rapport' => $this->rapportRepo
                                                    ->findByLatest()[0]->getId() ]);
    }

    /**
     * @Route("back-office/technicien/rapport={rapport}/intervention.html", 
     * name="technicien_equipements")
     */
    public function setEquipementRapport($rapport)
    {

        $rapportIntervention = $this->rapportRepo->findOneBy(['id' => $rapport]);
        
        $total = EquipementService::getTotal($rapportIntervention->getEquipements());

        $equipement = new Equipement();

        $builder = $this->createFormBuilder($equipement)
                            ->setAction($this->generateUrl('add_equipements'))
                            ->setMethod('POST')
                            ->add('designation') 
                            ->add('prixUnitaire', TextType::class)
                            ->add('quantite', TextType::class)
                            ->add('type', TextType::class)

                            ->add('rapport', HiddenType::class, 
                                ['data' => $rapportIntervention->getId() ])
                            ->add('save', SubmitType::class, 
                            ['label' => 'Ajouter un equipement', 'attr' => array('class' => 'btn btn-sm btn-primary mr-2')])
                            // ->add('save', SubmitType::class, ['label' => 'Poste'])
                            ->getForm();


        return $this->render('techniciens/rapport-equipement.html.twig', 
                                ['rapportIntervention' => $rapportIntervention,
                                'consomables' => $rapportIntervention->getEquipements(),
                                'formEquipement' => $builder->createView(),
                                'total' => $total
                            ]);
    }

    /**
     * @Route("back-office/technicien/consomables_intervention.html", 
     * name="add_equipements")
     */
    public function addConsomables(Request $request)
    {

       /// $data = $request->request->get('form');

       $consomable = new Equipement();

        $data = $this->cleanData($request->request->get('form'));

        $consomable->setDesignation($data['designation']);
        $consomable->setType($data['type']);
        $consomable->setPrixUnitaire($data['prixUnitaire']);
        $consomable->setQuantite($data['quantite']);

        $rapport = $this->rapportRepo->findOneBy(['id' => $data['rapport']]);
        $consomable->setRapport($rapport);

        $this->em->persist($consomable);
        $this->em->flush();

        return $this->redirectToRoute('technicien_equipements', ['rapport' => $data['rapport']]);
    }


     /**
     * @Route("back-office/technicien/dernier_rapport_intervention.html", 
     * name="equipement_dernier")
     */
    public function dernierRapport()
    {
        $dernierRapport = $this->rapportRepo->findByLatest()[0];
        
        $total = EquipementService::getTotal($dernierRapport->getEquipements());

        return $this->render('techniciens/rapport-confirmation.html.twig', [
            'rapportIntervention' => $dernierRapport,
            'total' => $total
        ]);
    }


    private function cleanData($data)
    {
        foreach ($data as $key => $value) {
            if ("save" == $key or "_token" == $key) {
               unset($data[$key]);
            }
        }

        return $data;
    }

}
