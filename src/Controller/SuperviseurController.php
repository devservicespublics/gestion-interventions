<?php

namespace App\Controller;

use App\Service\TService;
use App\Repository\RoleRepository;

use App\Repository\CompteRepository;
use App\Repository\AgencesRepository;
use JMS\Serializer\SerializerBuilder;
use App\Repository\InterventionRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\DemandeInterventionRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SuperviseurController extends AbstractController
{
   

    private $repoIntervention;
    private $repoDemandeIntervention;
    private $repoAgences;
    private $repoCompte;
 
    public function __construct(InterventionRepository $intervention, 
                                DemandeInterventionRepository $demande,
                                AgencesRepository $agences,
                                CompteRepository $compte)
    {
        $this->repoIntervention = $intervention;
        $this->repoDemandeIntervention = $demande;
        $this->repoAgences = $agences;
        $this->repoCompte = $compte;
    }
 
    /**
     * @Route("back-office/superviseurs", name="app_sup_homepage")
     */
    public function index()
    {
        $countAgences = $this->repoAgences->findByCount();
        $countDemande = $this->repoDemandeIntervention->findByCount();
        $countIntervention = $this->repoIntervention->findByCount();

        $countTech = TService::Count($this->repoCompte->findAll(), 'ROLE_TECHNICIEN');

        $series = [20000, 5000, 7000, 2000, 8000];

        return $this->render('superviseur/index.html.twig', [
            'countAgences' => $countAgences,
            'countDemandes' => $countDemande,
            'countIntervention' => $countIntervention,
            'countTechnicien' => $countTech,
            'series' => $series
        ]);
    }

    /**
     * @Route("back-office/statistique", name="superviseur_statistique")
     */
    public function getStatistique() : JsonResponse
    {

        $series = [5, 4, 3, 7];
        $serializer = SerializerBuilder::create()->build();
        $data = $serializer->serialize($series, JsonEncoder::FORMAT);

        return new JsonResponse($data, Response::HTTP_OK, [], true);

    }


    /**
     * @Route("/api/agences", name="trouvertouteslesagences")
     */
    public function findAllAgencies() : JsonResponse
    {
       
        $agences = $this->em->getRepository(Compte::class)
                            ->findOneBy(['email' => $this->getUser()->getEmail()]);

        $serializer = SerializerBuilder::create()->build();
        $data = $serializer->serialize($agences, JsonEncoder::FORMAT);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }
}
