<?php

namespace App\Controller;

use App\Entity\Role;
use App\Entity\Ville;
use App\Entity\Agences;
use App\Service\Dispatcher;
use App\Repository\RoleRepository;
use App\Entity\DemandeIntervention;
use App\Repository\VilleRepository;
use App\Repository\CompteRepository;
use App\Repository\DemandeInterventionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route("back-office")
 */
class DemandeInterventionController extends AbstractController
{

    private $em;
    private $repoVille;
    private $compteRepo;
    private $demandeRepo;

    public function __construct(EntityManagerInterface $manager, 
                                VilleRepository $repos,
                                CompteRepository $compte,
                                DemandeInterventionRepository $demande)
    {
        $this->em = $manager;
        $this->repoVille = $repos;
        $this->compteRepo = $compte;
        $this->demandeRepo = $demande;
    }

    // /**
    //  * @Route("/demande/demandes_intervention.html", name="demande_liste_intervention")
    //  */
    // public function index()
    // {
    //     return $this->render('demande_intervention/index.html.twig', [
    //         'controller_name' => 'DemandeInterventionController',
    //     ]);
    // }

    /**
     * @Route("/demande/historique_intervention.html", name="demande_historique")
     * @Route("/demande/{status}/demande_non_traitees.html", name="demande_active")
     */
    public function historique(Request $request, $status = null)
    {
      
      ///  $users = $this->compteRepo->findUserByRole();
       $enteteMessage = "Historique des demandes d'intervention";
       $demandeIntervention = $this->demandeRepo->findAll();
       if ($status) {
          $demandeIntervention = $this->demandeRepo->findOneByStatus($status);
          $enteteMessage = "Historique des demandes d'intervention non traitées";
       }
            
        return $this->render('interventions/historiques.html.twig', [
            'demande_interventions' => $demandeIntervention,
            'enteteMessage' => $enteteMessage
        ]);
    }

    /**
     * @Route("/demande/nouvelle_demande_intervention.html", name="demande_faire")
     * @Route("/demande/demande.html", name="intervention_ajouter")
     */
    public function demandeIntervention(Request $request)
    {
       
        $authUser = $this->getUser();

        $demandeIntervention = new DemandeIntervention();

        $builder = $this->createFormBuilder($demandeIntervention)

                ->setAction($this->generateUrl('intervention_ajouter'))
                ->setMethod('POST')
                ->add('description', TextareaType::class, [
                    'label' => 'Description du problème rencontré']
                )  
                ->add('typeMateriel', ChoiceType::class, [
                    'label' => 'Type équipement',
                    'choices' => [
                        "Ordinateur" => "Ordinateur",
                        "Imprimante" => "Imprimante",
                        "Autre" => "Autre"
                    ]
                    ]
                ) 
             
                ->add('origineProbleme', ChoiceType::class, [
                    'label' => 'Origine du problème rencontré',
                    'choices' => [
                        "Matériel" => "Matériel",
                        "Logiciel" => "Logiciel",
                        "Inconnue" => "Inconnue"
                    ]
                ]) 

                ->add('agences', ChoiceType::class, [
                    'label' => "Sélectionner l'agence ayant besoin d'assistance",
                    'choices' => $this->listVille($authUser),
                    'choice_label' => function(Agences $agence, $key, $value) {
                        return $agence->getNom();
                    },
                    'required' => true]
                )
                // ->add('status')
                ->add('save', SubmitType::class, ['label' => 'Poster la demande'])
                // ->add('cancel', SubmitType::class, ['label' => 'Annuler'])
                ->getForm();

                $builder->handleRequest($request);  

				if ($builder->isSubmitted() && $builder->isValid()) {
					$demandeIntervention->setStatus('En attente');

					if ('Autre' == $demandeIntervention->getTypeMateriel()) {
						$demandeIntervention->setNiveauAlerte(0);
					} else {
						$demandeIntervention->setNiveauAlerte(1);
					}

					$this->em->persist($demandeIntervention);
					$this->em->flush();

                    $notification = (new Dispatcher($this->compteRepo, $this->em))
                                ->setIntervention($demandeIntervention);

                    $this->addFlash('notification', $notification);

                    return $this->redirectToRoute('demande_historique');                    
				}

        return $this->render('interventions/demande-intervention.html.twig', [
            'formDemande' => $builder->createView()
        ]);
    }

    /**
     * @Route("/demande/carte_intervention.html", name="demande_carte_intervention")
     */
    public function carteIntervention()
    {

        $villeAgences = $this->getUser()->getUtilisateur()
                                         ->getVille()->getNomVille();

        $agences = $this->em->getRepository(Ville::class)
                            ->findOneBy(['nom_ville' => $villeAgences])
                            ->getAgences();   

        return $this->render('interventions/carte-agences.html.twig', [
            'agences' => $agences
        ]);
    }

    /**
     * @Route("/demande/derniere_demande_intervention.html", name="demande_derniere")
     */
    public function derniereDemande()
    {
        $derniereDemande = $this->demandeRepo->findByLatest();

        return $this->render('demande_intervention/derniere-demande.html.twig', [
            'demande' => $derniereDemande[0]
        ]);
    }
    
    private function listVille($authUser) 
    {
        $localisation = $authUser ? $authUser->getUtilisateur()
                            ->getVille()->getNomVille() : "Yaoundé";
                        
        return $this->repoVille
                    ->findOneByName($localisation)
                    ->getAgences();
    }
}
