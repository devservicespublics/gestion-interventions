<?php

namespace App\Controller;

use App\Entity\Ville;
use App\Entity\Compte;
use App\Entity\Agences;
use App\Form\AgenceType;
use App\Entity\Utilisateur;
use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\Types\TextType;
use App\Entity\DemandeIntervention;
use App\Repository\VilleRepository;
use App\Form\DemandeInterventionType;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\Form\FormBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("back-office")
 */
class AgencesController extends AbstractController
{

    private $em;
    private $repoVille;

    public function __construct(EntityManagerInterface $manager, 
                                VilleRepository $repos)
    {
        $this->em = $manager;
        $this->repoVille = $repos;
    }

    /**
     * @Route("/agences", name="app_agence_homepage")
     */
    public function index()
    {
        $villeAgences = $this->getUser()->getUtilisateur()
                                         ->getVille()->getNomVille();

        $agences = $this->em->getRepository(Ville::class)
                            ->findOneBy(['nom_ville' => $villeAgences])
                            ->getAgences();   

             
        return $this->render('agences/index.html.twig', [
            'agences' => $agences,
            'ville' => $villeAgences
        ]);
    }

 
    /**
     * @Route("/agences/ajouter_une_agence.html", name="agence_ajouter")
     */
    public function nouvelleAgence(Request $request)
    {

        $agence = new Agences();

        $ville = $this->getAuthUserVille();

        $agence = (new Agences())->setVille($ville);

        $builder = $this->createFormBuilder($agence)
        ->add('nom')
                    ->add('heureOuverture')
                    ->add('heureFermeture')
                    ->add('longitude')
                    ->add('latitude')
                    ->add('adresse')
                    ->add('save', SubmitType::class, ['label' => 'Ajouter une agence'])
                    ->getForm();

        $builder->handleRequest($request);  

        if ($builder->isSubmitted() && $builder->isValid()) {
            
            $this->em->persist($agence);
            $this->em->flush();

            return $this->redirectToRoute("app_agence_homepage");
        }
        return $this->render("agences/ajouter-agence.html.twig", [
            "formAgence" =>  $builder->createView(),
            "ville" => $ville->getNomVille()
        ]);
    }


    /**
     * @Route("/api/agences", name="trouvertouteslesagences")
     */
    public function findAllAgencies() : JsonResponse
    {
       
        $agences = $this->em->getRepository(Compte::class)
                            ->findOneBy(['email' => $this->getUser()->getEmail()]);

        $serializer = SerializerBuilder::create()->build();
        $data = $serializer->serialize($agences, JsonEncoder::FORMAT);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }


    private function getAuthUserVille() {

        return $this->getUser()->getUtilisateur()
                    ->getVille();
    }
}
