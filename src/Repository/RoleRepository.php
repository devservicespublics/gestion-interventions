<?php

namespace App\Repository;

use App\Entity\Role;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Role|null find($id, $lockMode = null, $lockVersion = null)
 * @method Role|null findOneBy(array $criteria, array $orderBy = null)
 * @method Role[]    findAll()
 * @method Role[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Role::class);
    }

    // /**
    //  * @return Role[] Returns an array of Role objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Role
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    
    public function findOneByName($nameRole): ?Role
    {

        return $this->createQueryBuilder('r')
            ->andWhere('r.label = :val')
            ->setParameter('val', $nameRole)
            ->getQuery()
            ->getOneOrNullResult();
            
       // return $this->findOneBy(["nom_ville" => $name_ville]);
    }


    public function findCountByName($nameRole)
    {

        return $this->createQueryBuilder('r')
            ->andWhere('r.label = :val')
            ->setParameter('val', $nameRole)
            ->select('COUNT(r)')
            ->getQuery()
            ->getSingleScalarResult();
            
       // return $this->findOneBy(["nom_ville" => $name_ville]);
    }
}
