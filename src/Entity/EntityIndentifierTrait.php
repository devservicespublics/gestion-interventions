<?php
namespace App\Entity;

use DateTime;
use Exception;
use Ramsey\Uuid\Uuid as uuid;
use Ramsey\Uuid\UuidInterface;
use Doctrine\ORM\Mapping as ORM;


trait EntityIndentifierTrait
{
    /**
     * The unique auto incremented primary key.
     *
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * The internal primary identity key.
     *
     * @var UuidInterface
     *
     * @ORM\Column(type="uuid", unique=true, nullable=true)
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    protected $uuid;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * 
     * @var Datetime|null
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * 
     * @var Datetime|null
     */
    protected $updatedAt;

    /**
    * Retourner la valeur auto incrementer de la table.
    *
    * @return int
    */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
    * Retourner le uuid de la table.
    *
    * @return UuidInterface
    */
    public function getUuid(): ?UuidInterface
    {
        return $this->uuid;
    }

    /**
    * Retourne la date de creation de l'entité.
    *
    * @return Datetime
    */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
    * Retourne la date de mise à jour de l'entité.
    *
    * @return Datetime
    */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     * 
     * @throws Exception;
    */
    public function onPrePersist(): void {
        $this->uuid = uuid::uuid4();
        $this->createdAt = new DateTime('NOW');
    }

    /**
     * @ORM\PreUpdate
     * 
     * @throws Exception;
    */
    public function onPreUpdate(): void {
        $this->updatedAt = new DateTime('NOW');
    }
}
