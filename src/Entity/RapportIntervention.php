<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RapportInterventionRepository")
 */
class RapportIntervention
{
   use EntityIndentifierTrait;
    /**
     * @ORM\Column(type="text")
     */
    private $descriptionSolution;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $etape;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Intervention", cascade={"persist", "remove"})
     */
    private $intervention;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Equipement", mappedBy="rapport", fetch="EAGER")
     */
    private $equipements;

    public function __construct()
    {
        $this->equipements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescriptionSolution(): ?string
    {
        return $this->descriptionSolution;
    }

    public function setDescriptionSolution(string $descriptionSolution): self
    {
        $this->descriptionSolution = $descriptionSolution;

        return $this;
    }

    public function getEtape(): ?int
    {
        return $this->etape;
    }

    public function setEtape(?int $etape): self
    {
        $this->etape = $etape;

        return $this;
    }

    public function getIntervention(): ?Intervention
    {
        return $this->intervention;
    }

    public function setIntervention(?Intervention $intervention): self
    {
        $this->intervention = $intervention;

        return $this;
    }

    /**
     * @return Collection|Equipement[]
     */
    public function getEquipements(): Collection
    {
        return $this->equipements;
    }

    public function addEquipement(Equipement $equipement): self
    {
        if (!$this->equipements->contains($equipement)) {
            $this->equipements[] = $equipement;
            $equipement->setRapport($this);
        }

        return $this;
    }

    public function removeEquipement(Equipement $equipement): self
    {
        if ($this->equipements->contains($equipement)) {
            $this->equipements->removeElement($equipement);
            // set the owning side to null (unless already changed)
            if ($equipement->getRapport() === $this) {
                $equipement->setRapport(null);
            }
        }

        return $this;
    }
}
