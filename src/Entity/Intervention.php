<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InterventionRepository")
 */
class Intervention
{
 

    use EntityIndentifierTrait;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $etat;

    /**
     * @ORM\Column(type="datetime", length=20)
     */
    private $dateIntervention;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\DemandeIntervention", 
     * inversedBy="intervention", 
     * cascade={"persist", "remove"}, fetch="EAGER")
     */
    private $demandeIntervention;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Utilisateur", inversedBy="interventions")
     */
    private $utilisateur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getDateIntervention(): ?string
    {
        return $this->dateIntervention;
    }

    public function setDateIntervention(\DateTime $dateIntervention= null): self
    {
        $this->dateIntervention = $dateIntervention;

        return $this;
    }

    public function getDemandeIntervention(): ?DemandeIntervention
    {
        return $this->demandeIntervention;
    }

    public function setDemandeIntervention(?DemandeIntervention $demandeIntervention): self
    {
        $this->demandeIntervention = $demandeIntervention;

        return $this;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?Utilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }
}
