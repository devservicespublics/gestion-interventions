<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompteRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="comptes_utilisateurs")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class Compte implements UserInterface
{
    use EntityIndentifierTrait;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Role", fetch="EAGER")
     * @ORM\JoinTable(name="comptes_roles", 
     *    joinColumns={@ORM\JoinColumn(name="compte", referencedColumnName="id")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="role", referencedColumnName="id")})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Utilisateur", mappedBy="compte", cascade={"persist", "remove"})
     */
    private $utilisateur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $user_role = $this->roles->toArray();
        $user_role = $user_role[0]->getLabel();

        // guarantee every user at least has ROLE_USER

        $roles[] = 'ROLE_USER';

        array_push($roles, $user_role);

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getByRole($role): ?self
    {
        if (! in_array($role, $this->getRoles())) {
            return null;
        } 

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?Utilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        // set (or unset) the owning side of the relation if necessary
        $newCompte = $utilisateur === null ? null : $this;
        if ($newCompte !== $utilisateur->getCompte()) {
            $utilisateur->setCompte($newCompte);
        }

        return $this;
    }
}
