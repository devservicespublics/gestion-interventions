<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DemandeInterventionRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\table(name="demandes_interventions")
 */
class DemandeIntervention
{
    use EntityIndentifierTrait;
    
    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="smallint")
     */
    private $niveau_alerte;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Agences", inversedBy="demandeInterventions")
     */
    private $agences;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $typeMateriel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $marqueMateriel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $origineProbleme;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Intervention", mappedBy="demandeIntervention", cascade={"persist", "remove"}, fetch="EAGER")
     */
    private $intervention;

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNiveauAlerte(): ?int
    {
        return $this->niveau_alerte;
    }

    public function setNiveauAlerte(int $niveau_alerte): self
    {
        $this->niveau_alerte = $niveau_alerte;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getAgences(): ?Agences
    {
        return $this->agences;
    }

    public function setAgences(?Agences $agences): self
    {
        $this->agences = $agences;

        return $this;
    }

    public function getTypeMateriel(): ?string
    {
        return $this->typeMateriel;
    }

    public function setTypeMateriel(string $typeMateriel): self
    {
        $this->typeMateriel = $typeMateriel;

        return $this;
    }

    public function getMarqueMateriel(): ?string
    {
        return $this->marqueMateriel;
    }

    public function setMarqueMateriel(?string $marqueMateriel): self
    {
        $this->marqueMateriel = $marqueMateriel;

        return $this;
    }

    public function getOrigineProbleme(): ?string
    {
        return $this->origineProbleme;
    }

    public function setOrigineProbleme(string $origineProbleme): self
    {
        $this->origineProbleme = $origineProbleme;

        return $this;
    }

    public function getIntervention(): ?Intervention
    {
        return $this->intervention;
    }

    public function setIntervention(?Intervention $intervention): self
    {
        $this->intervention = $intervention;

        // set (or unset) the owning side of the relation if necessary
        $newDemandeIntervention = $intervention === null ? null : $this;
        if ($newDemandeIntervention !== $intervention->getDemandeIntervention()) {
            $intervention->setDemandeIntervention($newDemandeIntervention);
        }

        return $this;
    }
}
