<?php

namespace App\Form;

use App\Entity\Agences;
use App\Entity\DemandeIntervention;
use App\Repository\VilleRepository;
use PhpParser\ErrorHandler\Collecting;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


class DemandeInterventionType extends AbstractType
{
    private $repoVille;

    public function __construct(VilleRepository $repos)
    {
        $this->repoVille = $repos;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        // $builder->add('agences', CollectionType::class, [
        //     'entry_type' => AgenceType::class
        // ]);

       
        $builder
            ->add('agences', EntityType::class, [
                'class' => Agences::class,
                'choice_label' => function(Agences $agence, $key, $value) {
                    return $agence->getNom(); 
                },
                'placeholder' => 'Liste des agences',
                'required' => true
            ])

            ->add('description')
            ->add('niveau_alerte')
            ->add('status')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DemandeIntervention::class,
        ]);
    }
}
