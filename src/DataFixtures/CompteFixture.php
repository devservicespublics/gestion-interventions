<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Compte;
use App\Entity\Utilisateur;
use App\Repository\RoleRepository;
use App\Repository\VilleRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Query\AST\Functions\UpperFunction;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CompteFixture extends Fixture
{
    private $passwordEncoder;
    protected $faker;
    private $villeRepository;
    private $roleRepository;
    private $position = false;
    
    public function __construct(UserPasswordEncoderInterface $pwdEncoder,
                                VilleRepository $villeRepository,
                                RoleRepository $roleRepository)
    {
        $this->passwordEncoder = $pwdEncoder;
        $this->villeRepository = $villeRepository;
        $this->roleRepository = $roleRepository;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        /// créer l'admin de Ydé
        
        $this->faker = Factory::create("fr_FR");

        $agent = $this->setUtilisateur();
        $manager->persist($agent);

        $profil = $this->setInfoUtilisateur();

        $profil->setCompte($agent);

        $manager->persist($profil);


        // creer un admin et lui affecter des techniciens
        $email = "sup@sygitech.cm";

        $role = "superviseur";

        $superversiseur = $this->setUtilisateur($email, $role);
        $manager->persist($superversiseur);

        $profil = $this->setInfoUtilisateur();

        $profil->setCompte($superversiseur);

        $manager->persist($profil);


        $this->position = true;

        for ($i=0; $i < 3; $i++) { 
            
            $email = "tech".($i+1)."user@sygitech.cm";
            $role = "technicien";

            $compteUtilisateur = $this->setUtilisateur($email, $role);
            $manager->persist($compteUtilisateur);

            // $superversiseur->setSubalterne($compteUtilisateur);

           // $superversiseur->setSubalterne($compteUtilisateur);
            
            $profil = $this->setInfoUtilisateur("Yes");

            $profil->setCompte($compteUtilisateur);

            $manager->persist($profil);
        }

        $manager->flush();
    }


    private function setUtilisateur($email = "agence.admin@sygitech.cm", 
                                    $role = "agence"): ?Compte
    {
        $user = new Compte();

        $user->setEmail($email);

        $user->setPassword($this->passwordEncoder
                        ->encodePassword($user, '123456'));

        $user->setRoles([$this->roleRepository
                                ->findOneByName('ROLE_'. strtoupper($role))]);

        return $user;
    }

    private function setInfoUtilisateur($position = "No") : ?Utilisateur
    {
        $utilisateur = new Utilisateur();

        $utilisateur->setNom($this->faker->name());
        $utilisateur->setSexe('M');
        $utilisateur->setPrenom($this->faker->lastName());
        $utilisateur->setDateDeNaissance($this->faker->dateTimeThisCentury() );
        $utilisateur->setTelephone($this->faker->mobileNumber());

        if ("Yes" == $position) {
    
            $utilisateur->setLatitude($this->faker->latitude());
            $utilisateur->setLongitude($this->faker->longitude());
        }

        $utilisateur->setVille($this->villeRepository->findOneByName());

        return $utilisateur;
    }
}
