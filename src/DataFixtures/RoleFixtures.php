<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RoleFixtures extends Fixture
{

    protected $faker;

    public function load(ObjectManager $manager)
    {
        /**
         * 
         * Nous avons trois roles 
         * SUPERVISEUR - gestion des techniciens et le suivi des interventions
         * TECHNICIEN - charger de faire les interventions; faire les rapports d'interventions
         * AGENCES - Remplir les demandes d'interventions
         * 
         */
        $roles = ['SUPERVISEUR', 'TECHNICIEN', 'AGENCE'];

        foreach ($roles as $key => $value) {
           $nouveauRole = new Role();
           $nouveauRole->setLabel('ROLE_'.$value);

          $manager->persist($nouveauRole);
        }

        $manager->flush();
    }
}
