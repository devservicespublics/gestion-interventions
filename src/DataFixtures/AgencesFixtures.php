<?php

namespace App\DataFixtures;

use App\Entity\Agences;
use App\Entity\Ville;
use Faker\Factory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;

class AgencesFixtures extends Fixture
{


    protected $faker;

    public function load(ObjectManager $manager)
    {
       

        $this->faker = Factory::create('fr_FR');

        $villesAgences = ['Yaoundé' => ['Nkolndongo', 'Ngoa Ekele', 'Biyem-Assi', 
                                        'Messa', 'Melen', 'Kondengui', 
                                        'Mokolo', 'Kenedy','Mvog-Ada', 
                                        'Elig Essono', 'Elig Edzoa', 'Elig Effa',
                                        'Tsinga', 'Awaé', 'Mimboman',
                                        'Mvolyé', 'Efoulan', 'Nsi Meyong', 
                                        'Quartier Briqueterie', 'Bastos', 'Ekié'], 

                            'Douala' => ['Akwa', 'Bonandjo', 'Bonapriso', 'Bonanjo', 
                                        'New Bell', 'Bonamoussadi', 'Deïdo', 'Bali'], 

                            'Bafoussam' => 'Bafoussam',
                            'Kribi' => 'Kribi', 
                            'Edéa' => 'Edéa'];

        foreach ($villesAgences as $localisation => $agences) {

            $ville = new Ville();
            $ville->setNomVille($localisation);
            $ville->setLatitude($this->faker->latitude());
            $ville->setLongitude($this->faker->longitude());
            $manager->persist($ville);

            if (is_array($agences)) {
                foreach ($agences as $key => $value) {

                    $agence = $this->setAgence($value, $ville);
                    $manager->persist($agence);
                }
            } else {
               
                $agence = $this->setAgence($value, $ville);
                $manager->persist($agence);
            }
        }
        
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }


    private function setAgence(String $nomAgence, Ville $ville) : Agences
    {
        $this->faker = Factory::create('fr_FR');

        $agence = new Agences();

        $agence->setVille($ville);
        $agence->setNom($nomAgence);
        $agence->setAdresse($this->faker->streetAddress());
        $agence->setHeureOuverture('08h00');
        $agence->setHeureFermeture('22h30');
        $agence->setLatitude($this->faker->latitude());
        $agence->setLongitude($this->faker->longitude());

        return $agence;


    }
}
