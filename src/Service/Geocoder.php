<?php 

namespace App\Service;

final class Geocoder
{
    private static $RAYON_DE_LA_TERRE = 6371;

    /***
     *  haversine formula to calculate the great-circle distance between two points – that is, the shortest 
     *  distance over the earth’s surface – 
     *  giving an ‘as-the-crow-flies’ distance between the points
     */
    public static function getHaversine(array $position1, array $position2)
    {
      $phi1 = $position1['latitude'];
      $phi2 = $position2['latitude'];

      $deltaPhi = $phi2 - $phi1;
      $deltaLambda = $position2['longitude'] - $position1['longitude'];

      $a = sin($deltaPhi/2) * sin($deltaPhi/2) + cos($phi1) * 
                            cos($phi2) *  sin($deltaLambda/2) * sin($deltaLambda/2);

      $c = 2 * atan2(sqrt($a), sqrt(1-$a));

      return self::$RAYON_DE_LA_TERRE * $c;
    }
}
