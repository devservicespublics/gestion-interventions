<?php


// TODO 

// Recuperer tous les techs dans la BD
// Creer un nouvelle instance d setIntervention
// Affecter l intervention a un tech en fonction de la distance

namespace App\Service;

use DateTime;
use App\Entity\Intervention;
use Doctrine\ORM\EntityManager;
use App\Entity\DemandeIntervention;
use App\Repository\CompteRepository;
use Doctrine\ORM\EntityManagerInterface;

final class Dispatcher 
{

    private $comptes;
    private $em;

    public function __construct(CompteRepository $repo, EntityManagerInterface $em)
    {
        $this->comptes = $repo;
        $this->em = $em;
    }

 function setIntervention(DemandeIntervention $demande)
    {
    
       $intervention = new Intervention();
       $intervention->setDateIntervention(new DateTime('NOW'));
       $intervention->setEtat("Encours");

       $demande->setIntervention($intervention);

       $positionAgence = [$demande->getAgences()->getLongitude(), 
                                        $demande->getAgences()->getLatitude()];
                
       $techniciens = $this->comptes->findTechUser();

       $positionAgence = ["longitude" => $demande->getAgences()->getLongitude(), 
                          "latitude"=> $demande->getAgences()->getLatitude()];

       $schedule = [];

       foreach ($techniciens as $key => $value) {
           if ($value) {

              $technicien = $value[0]->getUtilisateur();

              $positionTech = ["longitude" => $technicien->getLongitude(), 
                               "latitude"=> $technicien->getLatitude()];

              $distance = Geocoder::getHaversine($positionAgence, $positionTech);

              $schedule[] = ["distance" => $distance, "technicien" => $technicien];
             //  die();
           }
       }

       $tech = $this->courteDistance($schedule)['technicien'];
       $intervention->setUtilisateur($tech);

       $this->em->persist($intervention);
       $this->em->flush();

       return $this->setNotification($tech);
    
    //    $notification = "Le technicien ".$tech->getNom()."".$tech->getPrenom()."se sera dans vos locaux pour une intervention dans les plus brefs delais, vous pouvez le conctacter à l'adrrese suivante: ". $tech->getAdresse();
    //    return $notification
      // $schedule =  asort(, 'distance');
    }

    private function courteDistance(array $tab)
    {
        $min = $tab[0]['distance'];
        $index = 0;

        foreach ($tab as $key => $value) {
            if ($min >= $value['distance'] ) {

                $min = $tab[$key]['distance'];
                $index = $key;
            }
        }
        return $tab[$index];
    }

    private function setNotification($technicien) {
        $message = $technicien->getNom()." ". $technicien->getPrenom();
        $message .= " sera dans vos locaux dans les brefs délais pour une intervention.";
        $message .= " Vous pouvez le contacter au ".$technicien->getTelephone();
        return $message;
    }
}
