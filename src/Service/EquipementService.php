<?php

namespace App\Service;

use App\Entity\Equipement;

final class EquipementService
{
    public static function getTotal($consomables)
    {
        $total = 0;

        foreach ($consomables as $key => $consomable) {
            $total += $consomable->getPrixUnitaire() * $consomable->getQuantite();
        }
        
        return $total;
    }
}
