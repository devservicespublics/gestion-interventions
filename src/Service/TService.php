<?php

namespace App\Service;

final class TService 
{
    static public function Count($Tab, $parametre)
    {
        $countTech = 0;
        foreach ($Tab as $key => $item) {
            if (in_array('ROLE_TECHNICIEN' ,$item->getRoles()) ) {
                $countTech++;
            }
        }
        
        return $countTech;
    }
}
