<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191002200134 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_497B315ED17F50A6');
        $this->addSql('DROP INDEX UNIQ_497B315EF2C56620');
        $this->addSql('DROP INDEX IDX_497B315EA73F0036');
        $this->addSql('CREATE TEMPORARY TABLE __temp__utilisateurs AS SELECT id, compte_id, ville_id, date_de_naissance, created_at, updated_at, uuid, nom, prenom, sexe, telephone, longitude, latitude FROM utilisateurs');
        $this->addSql('DROP TABLE utilisateurs');
        $this->addSql('CREATE TABLE utilisateurs (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, compte_id INTEGER UNSIGNED DEFAULT NULL, ville_id INTEGER UNSIGNED DEFAULT NULL, date_de_naissance DATE NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, uuid CHAR(36) DEFAULT NULL COLLATE BINARY --(DC2Type:uuid)
        , nom VARCHAR(50) NOT NULL COLLATE BINARY, prenom VARCHAR(150) NOT NULL COLLATE BINARY, sexe VARCHAR(1) NOT NULL COLLATE BINARY, telephone VARCHAR(20) NOT NULL COLLATE BINARY, longitude VARCHAR(255) DEFAULT NULL COLLATE BINARY, latitude VARCHAR(255) DEFAULT NULL COLLATE BINARY, CONSTRAINT FK_497B315EF2C56620 FOREIGN KEY (compte_id) REFERENCES comptes_utilisateurs (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_497B315EA73F0036 FOREIGN KEY (ville_id) REFERENCES villes (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO utilisateurs (id, compte_id, ville_id, date_de_naissance, created_at, updated_at, uuid, nom, prenom, sexe, telephone, longitude, latitude) SELECT id, compte_id, ville_id, date_de_naissance, created_at, updated_at, uuid, nom, prenom, sexe, telephone, longitude, latitude FROM __temp__utilisateurs');
        $this->addSql('DROP TABLE __temp__utilisateurs');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_497B315ED17F50A6 ON utilisateurs (uuid)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_497B315EF2C56620 ON utilisateurs (compte_id)');
        $this->addSql('CREATE INDEX IDX_497B315EA73F0036 ON utilisateurs (ville_id)');
        $this->addSql('DROP INDEX IDX_AD7D62E71E969C5');
        $this->addSql('DROP INDEX IDX_AD7D62E75D1FF8FF');
        $this->addSql('CREATE TEMPORARY TABLE __temp__subalternes AS SELECT utilisateurs_id, utilisateurs_subalterne_id FROM subalternes');
        $this->addSql('DROP TABLE subalternes');
        $this->addSql('CREATE TABLE subalternes (utilisateurs_id INTEGER UNSIGNED NOT NULL, utilisateurs_subalterne_id INTEGER UNSIGNED NOT NULL, PRIMARY KEY(utilisateurs_id, utilisateurs_subalterne_id), CONSTRAINT FK_AD7D62E71E969C5 FOREIGN KEY (utilisateurs_id) REFERENCES utilisateurs (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_AD7D62E75D1FF8FF FOREIGN KEY (utilisateurs_subalterne_id) REFERENCES utilisateurs (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO subalternes (utilisateurs_id, utilisateurs_subalterne_id) SELECT utilisateurs_id, utilisateurs_subalterne_id FROM __temp__subalternes');
        $this->addSql('DROP TABLE __temp__subalternes');
        $this->addSql('CREATE INDEX IDX_AD7D62E71E969C5 ON subalternes (utilisateurs_id)');
        $this->addSql('CREATE INDEX IDX_AD7D62E75D1FF8FF ON subalternes (utilisateurs_subalterne_id)');
        $this->addSql('DROP INDEX UNIQ_F36A4EF6D17F50A6');
        $this->addSql('DROP INDEX IDX_F36A4EF69917E4AB');
        $this->addSql('CREATE TEMPORARY TABLE __temp__demandes_interventions AS SELECT id, agences_id, description, niveau_alerte, uuid, created_at, updated_at, status, type_materiel, marque_materiel, origine_probleme FROM demandes_interventions');
        $this->addSql('DROP TABLE demandes_interventions');
        $this->addSql('CREATE TABLE demandes_interventions (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, agences_id INTEGER UNSIGNED DEFAULT NULL, description CLOB NOT NULL COLLATE BINARY, niveau_alerte SMALLINT NOT NULL, uuid CHAR(36) DEFAULT NULL COLLATE BINARY --(DC2Type:uuid)
        , created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, status VARCHAR(100) NOT NULL COLLATE BINARY, type_materiel VARCHAR(100) NOT NULL COLLATE BINARY, marque_materiel VARCHAR(255) DEFAULT NULL COLLATE BINARY, origine_probleme VARCHAR(255) NOT NULL COLLATE BINARY, CONSTRAINT FK_F36A4EF69917E4AB FOREIGN KEY (agences_id) REFERENCES agences (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO demandes_interventions (id, agences_id, description, niveau_alerte, uuid, created_at, updated_at, status, type_materiel, marque_materiel, origine_probleme) SELECT id, agences_id, description, niveau_alerte, uuid, created_at, updated_at, status, type_materiel, marque_materiel, origine_probleme FROM __temp__demandes_interventions');
        $this->addSql('DROP TABLE __temp__demandes_interventions');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F36A4EF6D17F50A6 ON demandes_interventions (uuid)');
        $this->addSql('CREATE INDEX IDX_F36A4EF69917E4AB ON demandes_interventions (agences_id)');
        $this->addSql('DROP INDEX IDX_64D0B3D1F2C56620');
        $this->addSql('DROP INDEX IDX_64D0B3D1D60322AC');
        $this->addSql('CREATE TEMPORARY TABLE __temp__role_compte AS SELECT role_id, compte_id FROM role_compte');
        $this->addSql('DROP TABLE role_compte');
        $this->addSql('CREATE TABLE role_compte (role_id INTEGER UNSIGNED NOT NULL, compte_id INTEGER UNSIGNED NOT NULL, PRIMARY KEY(role_id, compte_id), CONSTRAINT FK_64D0B3D1D60322AC FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_64D0B3D1F2C56620 FOREIGN KEY (compte_id) REFERENCES comptes_utilisateurs (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO role_compte (role_id, compte_id) SELECT role_id, compte_id FROM __temp__role_compte');
        $this->addSql('DROP TABLE __temp__role_compte');
        $this->addSql('CREATE INDEX IDX_64D0B3D1F2C56620 ON role_compte (compte_id)');
        $this->addSql('CREATE INDEX IDX_64D0B3D1D60322AC ON role_compte (role_id)');
        $this->addSql('DROP INDEX UNIQ_B46015DDD17F50A6');
        $this->addSql('DROP INDEX IDX_B46015DDA73F0036');
        $this->addSql('CREATE TEMPORARY TABLE __temp__agences AS SELECT id, ville_id, created_at, updated_at, uuid, adresse, nom, longitude, latitude, heure_fermeture, heure_overture FROM agences');
        $this->addSql('DROP TABLE agences');
        $this->addSql('CREATE TABLE agences (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ville_id INTEGER UNSIGNED DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, uuid CHAR(36) DEFAULT NULL COLLATE BINARY --(DC2Type:uuid)
        , adresse VARCHAR(255) NOT NULL COLLATE BINARY, nom VARCHAR(100) NOT NULL COLLATE BINARY, longitude VARCHAR(255) NOT NULL COLLATE BINARY, latitude VARCHAR(255) NOT NULL COLLATE BINARY, heure_fermeture VARCHAR(255) NOT NULL COLLATE BINARY, heure_overture VARCHAR(255) NOT NULL COLLATE BINARY, CONSTRAINT FK_B46015DDA73F0036 FOREIGN KEY (ville_id) REFERENCES villes (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO agences (id, ville_id, created_at, updated_at, uuid, adresse, nom, longitude, latitude, heure_fermeture, heure_overture) SELECT id, ville_id, created_at, updated_at, uuid, adresse, nom, longitude, latitude, heure_fermeture, heure_overture FROM __temp__agences');
        $this->addSql('DROP TABLE __temp__agences');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B46015DDD17F50A6 ON agences (uuid)');
        $this->addSql('CREATE INDEX IDX_B46015DDA73F0036 ON agences (ville_id)');
        $this->addSql('ALTER TABLE rapport_intervention ADD COLUMN etape SMALLINT DEFAULT NULL');
        $this->addSql('DROP INDEX IDX_EE82C88457698A6A');
        $this->addSql('DROP INDEX IDX_EE82C884CFF65260');
        $this->addSql('CREATE TEMPORARY TABLE __temp__comptes_roles AS SELECT compte, role FROM comptes_roles');
        $this->addSql('DROP TABLE comptes_roles');
        $this->addSql('CREATE TABLE comptes_roles (compte INTEGER UNSIGNED NOT NULL, role INTEGER UNSIGNED NOT NULL, PRIMARY KEY(compte, role), CONSTRAINT FK_EE82C884CFF65260 FOREIGN KEY (compte) REFERENCES comptes_utilisateurs (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_EE82C88457698A6A FOREIGN KEY (role) REFERENCES roles (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO comptes_roles (compte, role) SELECT compte, role FROM __temp__comptes_roles');
        $this->addSql('DROP TABLE __temp__comptes_roles');
        $this->addSql('CREATE INDEX IDX_EE82C88457698A6A ON comptes_roles (role)');
        $this->addSql('CREATE INDEX IDX_EE82C884CFF65260 ON comptes_roles (compte)');
        $this->addSql('DROP INDEX UNIQ_B8B4C6F3D17F50A6');
        $this->addSql('CREATE TEMPORARY TABLE __temp__equipement AS SELECT id, designation, prix_unitaire, quantite, type, uuid, created_at, updated_at FROM equipement');
        $this->addSql('DROP TABLE equipement');
        $this->addSql('CREATE TABLE equipement (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, prix_unitaire INTEGER NOT NULL, quantite SMALLINT NOT NULL, uuid CHAR(36) DEFAULT NULL COLLATE BINARY --(DC2Type:uuid)
        , created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, designation VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO equipement (id, designation, prix_unitaire, quantite, type, uuid, created_at, updated_at) SELECT id, designation, prix_unitaire, quantite, type, uuid, created_at, updated_at FROM __temp__equipement');
        $this->addSql('DROP TABLE __temp__equipement');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B8B4C6F3D17F50A6 ON equipement (uuid)');
        $this->addSql('DROP INDEX UNIQ_D11814ABD17F50A6');
        $this->addSql('DROP INDEX UNIQ_D11814AB7607473E');
        $this->addSql('DROP INDEX IDX_D11814ABFB88E14F');
        $this->addSql('CREATE TEMPORARY TABLE __temp__intervention AS SELECT id, demande_intervention_id, utilisateur_id, date_intervention, uuid, created_at, updated_at, etat FROM intervention');
        $this->addSql('DROP TABLE intervention');
        $this->addSql('CREATE TABLE intervention (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, demande_intervention_id INTEGER UNSIGNED DEFAULT NULL, utilisateur_id INTEGER UNSIGNED DEFAULT NULL, date_intervention DATETIME NOT NULL, uuid CHAR(36) DEFAULT NULL COLLATE BINARY --(DC2Type:uuid)
        , created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, etat VARCHAR(100) NOT NULL COLLATE BINARY, CONSTRAINT FK_D11814AB7607473E FOREIGN KEY (demande_intervention_id) REFERENCES demandes_interventions (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_D11814ABFB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateurs (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO intervention (id, demande_intervention_id, utilisateur_id, date_intervention, uuid, created_at, updated_at, etat) SELECT id, demande_intervention_id, utilisateur_id, date_intervention, uuid, created_at, updated_at, etat FROM __temp__intervention');
        $this->addSql('DROP TABLE __temp__intervention');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D11814ABD17F50A6 ON intervention (uuid)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D11814AB7607473E ON intervention (demande_intervention_id)');
        $this->addSql('CREATE INDEX IDX_D11814ABFB88E14F ON intervention (utilisateur_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_B46015DDD17F50A6');
        $this->addSql('DROP INDEX IDX_B46015DDA73F0036');
        $this->addSql('CREATE TEMPORARY TABLE __temp__agences AS SELECT id, ville_id, adresse, nom, heure_overture, heure_fermeture, longitude, latitude, uuid, created_at, updated_at FROM agences');
        $this->addSql('DROP TABLE agences');
        $this->addSql('CREATE TABLE agences (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ville_id INTEGER UNSIGNED DEFAULT NULL, adresse VARCHAR(255) NOT NULL, nom VARCHAR(100) NOT NULL, heure_overture VARCHAR(255) NOT NULL, heure_fermeture VARCHAR(255) NOT NULL, longitude VARCHAR(255) NOT NULL, latitude VARCHAR(255) NOT NULL, uuid CHAR(36) DEFAULT NULL --(DC2Type:uuid)
        , created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL)');
        $this->addSql('INSERT INTO agences (id, ville_id, adresse, nom, heure_overture, heure_fermeture, longitude, latitude, uuid, created_at, updated_at) SELECT id, ville_id, adresse, nom, heure_overture, heure_fermeture, longitude, latitude, uuid, created_at, updated_at FROM __temp__agences');
        $this->addSql('DROP TABLE __temp__agences');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B46015DDD17F50A6 ON agences (uuid)');
        $this->addSql('CREATE INDEX IDX_B46015DDA73F0036 ON agences (ville_id)');
        $this->addSql('DROP INDEX IDX_EE82C884CFF65260');
        $this->addSql('DROP INDEX IDX_EE82C88457698A6A');
        $this->addSql('CREATE TEMPORARY TABLE __temp__comptes_roles AS SELECT compte, role FROM comptes_roles');
        $this->addSql('DROP TABLE comptes_roles');
        $this->addSql('CREATE TABLE comptes_roles (compte INTEGER UNSIGNED NOT NULL, role INTEGER UNSIGNED NOT NULL, PRIMARY KEY(compte, role))');
        $this->addSql('INSERT INTO comptes_roles (compte, role) SELECT compte, role FROM __temp__comptes_roles');
        $this->addSql('DROP TABLE __temp__comptes_roles');
        $this->addSql('CREATE INDEX IDX_EE82C884CFF65260 ON comptes_roles (compte)');
        $this->addSql('CREATE INDEX IDX_EE82C88457698A6A ON comptes_roles (role)');
        $this->addSql('DROP INDEX UNIQ_F36A4EF6D17F50A6');
        $this->addSql('DROP INDEX IDX_F36A4EF69917E4AB');
        $this->addSql('CREATE TEMPORARY TABLE __temp__demandes_interventions AS SELECT id, agences_id, description, niveau_alerte, status, type_materiel, marque_materiel, origine_probleme, uuid, created_at, updated_at FROM demandes_interventions');
        $this->addSql('DROP TABLE demandes_interventions');
        $this->addSql('CREATE TABLE demandes_interventions (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, agences_id INTEGER UNSIGNED DEFAULT NULL, description CLOB NOT NULL, niveau_alerte SMALLINT NOT NULL, status VARCHAR(100) NOT NULL, type_materiel VARCHAR(100) NOT NULL, marque_materiel VARCHAR(255) DEFAULT NULL, origine_probleme VARCHAR(255) NOT NULL, uuid CHAR(36) DEFAULT NULL --(DC2Type:uuid)
        , created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL)');
        $this->addSql('INSERT INTO demandes_interventions (id, agences_id, description, niveau_alerte, status, type_materiel, marque_materiel, origine_probleme, uuid, created_at, updated_at) SELECT id, agences_id, description, niveau_alerte, status, type_materiel, marque_materiel, origine_probleme, uuid, created_at, updated_at FROM __temp__demandes_interventions');
        $this->addSql('DROP TABLE __temp__demandes_interventions');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F36A4EF6D17F50A6 ON demandes_interventions (uuid)');
        $this->addSql('CREATE INDEX IDX_F36A4EF69917E4AB ON demandes_interventions (agences_id)');
        $this->addSql('DROP INDEX UNIQ_B8B4C6F3D17F50A6');
        $this->addSql('CREATE TEMPORARY TABLE __temp__equipement AS SELECT id, designation, prix_unitaire, quantite, type, uuid, created_at, updated_at FROM equipement');
        $this->addSql('DROP TABLE equipement');
        $this->addSql('CREATE TABLE equipement (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, prix_unitaire INTEGER NOT NULL, quantite SMALLINT NOT NULL, uuid CHAR(36) DEFAULT NULL --(DC2Type:uuid)
        , created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, designation CHAR(36) NOT NULL COLLATE BINARY --(DC2Type:uuid)
        , type CHAR(36) NOT NULL COLLATE BINARY --(DC2Type:uuid)
        )');
        $this->addSql('INSERT INTO equipement (id, designation, prix_unitaire, quantite, type, uuid, created_at, updated_at) SELECT id, designation, prix_unitaire, quantite, type, uuid, created_at, updated_at FROM __temp__equipement');
        $this->addSql('DROP TABLE __temp__equipement');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B8B4C6F3D17F50A6 ON equipement (uuid)');
        $this->addSql('DROP INDEX UNIQ_D11814ABD17F50A6');
        $this->addSql('DROP INDEX UNIQ_D11814AB7607473E');
        $this->addSql('DROP INDEX IDX_D11814ABFB88E14F');
        $this->addSql('CREATE TEMPORARY TABLE __temp__intervention AS SELECT id, demande_intervention_id, utilisateur_id, etat, date_intervention, uuid, created_at, updated_at FROM intervention');
        $this->addSql('DROP TABLE intervention');
        $this->addSql('CREATE TABLE intervention (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, demande_intervention_id INTEGER UNSIGNED DEFAULT NULL, utilisateur_id INTEGER UNSIGNED DEFAULT NULL, etat VARCHAR(100) NOT NULL, date_intervention DATETIME NOT NULL, uuid CHAR(36) DEFAULT NULL --(DC2Type:uuid)
        , created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL)');
        $this->addSql('INSERT INTO intervention (id, demande_intervention_id, utilisateur_id, etat, date_intervention, uuid, created_at, updated_at) SELECT id, demande_intervention_id, utilisateur_id, etat, date_intervention, uuid, created_at, updated_at FROM __temp__intervention');
        $this->addSql('DROP TABLE __temp__intervention');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D11814ABD17F50A6 ON intervention (uuid)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D11814AB7607473E ON intervention (demande_intervention_id)');
        $this->addSql('CREATE INDEX IDX_D11814ABFB88E14F ON intervention (utilisateur_id)');
        $this->addSql('DROP INDEX UNIQ_75EE7B38D17F50A6');
        $this->addSql('CREATE TEMPORARY TABLE __temp__rapport_intervention AS SELECT id, description_solution, uuid, created_at, updated_at FROM rapport_intervention');
        $this->addSql('DROP TABLE rapport_intervention');
        $this->addSql('CREATE TABLE rapport_intervention (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, description_solution CLOB NOT NULL, uuid CHAR(36) DEFAULT NULL --(DC2Type:uuid)
        , created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL)');
        $this->addSql('INSERT INTO rapport_intervention (id, description_solution, uuid, created_at, updated_at) SELECT id, description_solution, uuid, created_at, updated_at FROM __temp__rapport_intervention');
        $this->addSql('DROP TABLE __temp__rapport_intervention');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_75EE7B38D17F50A6 ON rapport_intervention (uuid)');
        $this->addSql('DROP INDEX IDX_64D0B3D1D60322AC');
        $this->addSql('DROP INDEX IDX_64D0B3D1F2C56620');
        $this->addSql('CREATE TEMPORARY TABLE __temp__role_compte AS SELECT role_id, compte_id FROM role_compte');
        $this->addSql('DROP TABLE role_compte');
        $this->addSql('CREATE TABLE role_compte (role_id INTEGER UNSIGNED NOT NULL, compte_id INTEGER UNSIGNED NOT NULL, PRIMARY KEY(role_id, compte_id))');
        $this->addSql('INSERT INTO role_compte (role_id, compte_id) SELECT role_id, compte_id FROM __temp__role_compte');
        $this->addSql('DROP TABLE __temp__role_compte');
        $this->addSql('CREATE INDEX IDX_64D0B3D1D60322AC ON role_compte (role_id)');
        $this->addSql('CREATE INDEX IDX_64D0B3D1F2C56620 ON role_compte (compte_id)');
        $this->addSql('DROP INDEX IDX_AD7D62E71E969C5');
        $this->addSql('DROP INDEX IDX_AD7D62E75D1FF8FF');
        $this->addSql('CREATE TEMPORARY TABLE __temp__subalternes AS SELECT utilisateurs_id, utilisateurs_subalterne_id FROM subalternes');
        $this->addSql('DROP TABLE subalternes');
        $this->addSql('CREATE TABLE subalternes (utilisateurs_id INTEGER UNSIGNED NOT NULL, utilisateurs_subalterne_id INTEGER UNSIGNED NOT NULL, PRIMARY KEY(utilisateurs_id, utilisateurs_subalterne_id))');
        $this->addSql('INSERT INTO subalternes (utilisateurs_id, utilisateurs_subalterne_id) SELECT utilisateurs_id, utilisateurs_subalterne_id FROM __temp__subalternes');
        $this->addSql('DROP TABLE __temp__subalternes');
        $this->addSql('CREATE INDEX IDX_AD7D62E71E969C5 ON subalternes (utilisateurs_id)');
        $this->addSql('CREATE INDEX IDX_AD7D62E75D1FF8FF ON subalternes (utilisateurs_subalterne_id)');
        $this->addSql('DROP INDEX UNIQ_497B315ED17F50A6');
        $this->addSql('DROP INDEX UNIQ_497B315EF2C56620');
        $this->addSql('DROP INDEX IDX_497B315EA73F0036');
        $this->addSql('CREATE TEMPORARY TABLE __temp__utilisateurs AS SELECT id, compte_id, ville_id, nom, prenom, sexe, date_de_naissance, telephone, longitude, latitude, uuid, created_at, updated_at FROM utilisateurs');
        $this->addSql('DROP TABLE utilisateurs');
        $this->addSql('CREATE TABLE utilisateurs (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, compte_id INTEGER UNSIGNED DEFAULT NULL, ville_id INTEGER UNSIGNED DEFAULT NULL, nom VARCHAR(50) NOT NULL, prenom VARCHAR(150) NOT NULL, sexe VARCHAR(1) NOT NULL, date_de_naissance DATE NOT NULL, telephone VARCHAR(20) NOT NULL, longitude VARCHAR(255) DEFAULT NULL, latitude VARCHAR(255) DEFAULT NULL, uuid CHAR(36) DEFAULT NULL --(DC2Type:uuid)
        , created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL)');
        $this->addSql('INSERT INTO utilisateurs (id, compte_id, ville_id, nom, prenom, sexe, date_de_naissance, telephone, longitude, latitude, uuid, created_at, updated_at) SELECT id, compte_id, ville_id, nom, prenom, sexe, date_de_naissance, telephone, longitude, latitude, uuid, created_at, updated_at FROM __temp__utilisateurs');
        $this->addSql('DROP TABLE __temp__utilisateurs');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_497B315ED17F50A6 ON utilisateurs (uuid)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_497B315EF2C56620 ON utilisateurs (compte_id)');
        $this->addSql('CREATE INDEX IDX_497B315EA73F0036 ON utilisateurs (ville_id)');
    }
}
